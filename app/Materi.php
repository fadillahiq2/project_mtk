<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $fillable = [
        'title', 'icon'
    ];


    public function soal()
    {
        return $this->hasMany(Soal::class, 'materi_id');
    }

    public function sub_materi()
    {
        return $this->hasMany(SubMateri::class, 'materi_id');
    }

    public function materi_images()
    {
        return $this->hasMany(MateriImages::class, 'materi_id');
    }


    public function sub_materi_images()
    {
        return $this->hasMany(SubMateriImages::class, 'materi_id');
    }


}
