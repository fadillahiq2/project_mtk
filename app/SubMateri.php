<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMateri extends Model
{
    protected $fillable = [
        'title', 'description'
    ];

    public function materis()
    {
        return $this->belongsTo(Materi::class, 'materi_id', 'id');
    }
}
