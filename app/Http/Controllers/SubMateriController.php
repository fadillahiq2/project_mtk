<?php

namespace App\Http\Controllers;

use App\Materi;
use App\SubMateri;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\AssignOp\BitwiseOr;

class SubMateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submateris = SubMateri::with('materis')->paginate(5);

        return view('submateri.index', compact('submateris'),[
            "title" => "Daftar Sub Materi"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materis = Materi::all();

        return view('submateri.create', compact('materis'),[
            "title" => "Tambah Sub Materi"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'materi_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);

        $submateri = new SubMateri;
        $submateri->materi_id = $request->materi_id;
        $submateri->title = $request->title;
        $submateri->description = $request->description;
        $submateri->save();

        return redirect()->route('sub-materi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submateri = SubMateri::find($id);
        $materis = Materi::all();
        return view('submateri.edit', compact('submateri', 'materis'),[
            "title" => "Edit Sub Materi"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'materi_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);

        $submateri = SubMateri::find($id);
        $submateri->materi_id = $request->materi_id;
        $submateri->title = $request->title;
        $submateri->description = $request->description;
        $submateri->save();

        return redirect()->route('sub-materi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubMateri $subMateri)
    {
        $subMateri->delete();

        return redirect()->route('sub-materi.index');
    }
}
