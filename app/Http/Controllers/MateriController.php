<?php

namespace App\Http\Controllers;

use App\Materi;
use App\MateriImages;
use App\Soal;
use App\SubMateri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
class MateriController extends Controller
{
    public function index($id)
    {

        $materi = Materi::find($id);

        $sub_materi = SubMateri::with('materis')->where('materi_id', $id)->get();
        $materi_images = MateriImages::where('materi_id', $id)->get();


        return view('user.materi', compact('materi', 'sub_materi', 'materi_images', ));
    }

    public function soal(Request $request,$id)
    {
        $soal = Soal::with('materi')->where('materi_id', $id)->inRandomOrder()->get();
        $materi = Materi::find($id);

        return view('user.latihan', compact('soal', 'materi'))->with('i');
    }

    public function user()
    {
        $materi = Materi::get();

        return view('user.index', compact('materi'));
    }

    public function nilai(Request $request)
    {
        $url = $request->url;

        $soal = Soal::where('materi_id', $url)->count();
        $all = $request->all();
        $jml = 0;
        $hasil = 0;
        if ($request->pilihan1 == null){

        }elseif ($request->pilihan1 === $request->jawaban1 ) {
            $jml=$jml + 1;
        }
        if ($request->pilihan2 == null) {

        }elseif ($request->pilihan2 === $request->jawaban2) {
            $jml = $jml + 1;
        }
        if ($request->pilihan3 == null) {

        }elseif ($request->pilihan3 === $request->jawaban3) {
            $jml = $jml + 1;
        }
        if ($request->pilihan4 == null) {

        }elseif ($request->pilihan4 === $request->jawaban4) {
            $jml = $jml + 1;
        }
        if ($request->pilihan5 == null) {

        }elseif ($request->pilihan5 === $request->jawaban5) {
            $jml = $jml + 1;
        }
        if ($request->pilihan6 == null) {

        }elseif ($request->pilihan6 === $request->jawaban6) {
            $jml = $jml + 1;
        }
        if ($request->pilihan7 == null) {

        }elseif ($request->pilihan7 === $request->jawaban7) {
            $jml = $jml + 1;
        }
        if ($request->pilihan8 == null) {

        }elseif ($request->pilihan8 === $request->jawaban8) {
            $jml = $jml + 1;
        }
        if ($request->pilihan9 == null) {

        }elseif ($request->pilihan9 === $request->jawaban9) {
            $jml = $jml + 1;
        }
        if ($request->pilihan10 == null) {

        }elseif ($request->pilihan10 === $request->jawaban10) {
            $jml = $jml + 1;
        }

        $request->validate([
            'pilihan1' => 'required',
            'pilihan2' => 'required',
            'pilihan3' => 'required',
            'pilihan4' => 'required',
            'pilihan5' => 'required',
            'pilihan6' => 'required',
            'pilihan7' => 'required',
            'pilihan8' => 'required',
            'pilihan9' => 'required',
            'pilihan10' => 'required',
        ]);

        $hasil = ($jml / $soal) * 100;

        if ($jml === 0) {
            Alert::success('Hasil','0');
            return redirect()->route('materi.latihan',$url);
        }else{
            $hasil = ($jml / $soal) * 100;

            Alert::success('Hasil',$hasil);
            return redirect()->route('materi.latihan',$url);
        }

    }

//     public function score(Request $request)
//     {

//         $nilai = DB::table('materis')
//             ->join('soals', 'materis.id', '=', 'soals.materi_id')
//             ->where('materi_id', 1)
//             ->select('jawaban_benar')
//             ->get();
//             $score = 0;
//         foreach ($nilai as $key => $value) {
//             # code...
//         }
//         if ($request == $nilai)
//         {

//         }
//         return view('limit.latihan',compact('score'));
//     }
}
