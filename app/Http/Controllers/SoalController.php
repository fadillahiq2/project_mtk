<?php

namespace App\Http\Controllers;

use App\Materi;
use App\Soal;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soals = Soal::with('materi')->latest()->paginate(5);

        return view('soal.index', compact('soals'),[
            "title" => "Daftar Soal"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materis = Materi::all();

        return view('soal.create', compact('materis'),[
            "title" => "Tambah Soal"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'materi_id' => 'required',
            'pertanyaan' => 'required',
            'a' => 'required',
            'b' => 'required',
            'c' => 'required',
            'd' => 'required',
            'e' => 'required',
            'jawaban_benar' => 'required'
        ]);

        $batas = Soal::with('materi')->where('materi_id', $request->materi_id)->count();
        if($batas >= 10){
            return redirect()->route('soal.index')->with('failed', 'Maaf Soal Tidak Bisa Lebih Dari 10');
        }else{



        $soal = new Soal;
        $soal->materi_id = $request->materi_id;
        $soal->pertanyaan = $request->pertanyaan;
        $soal->a = $request->a;
        $soal->b = $request->b;
        $soal->c = $request->c;
        $soal->d = $request->d;
        $soal->e = $request->e;
        $soal->jawaban_benar = $request->jawaban_benar;
        $soal->save();

        return redirect()->route('soal.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Soal $soal)
    {
        $materis = Materi::paginate(10);

        return view('soal.edit', compact('soal', 'materis'),[
            "title" => "Edit Soal"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'materi_id' => 'required',
            'a' => 'required',
            'b' => 'required',
            'c' => 'required',
            'd' => 'required',
            'e' => 'required',
            'jawaban_benar' => 'required',
        ]);

        $soal = Soal::find($id);
        $soal->pertanyaan = $request->pertanyaan;
        $soal->materi_id = $request->materi_id;
        $soal->a = $request->a;
        $soal->b = $request->b;
        $soal->c = $request->c;
        $soal->d = $request->d;
        $soal->e = $request->e;
        $soal->jawaban_benar = $request->jawaban_benar;
        $soal->save();

        return redirect()->route('soal.index')
                        ->with('success','Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Soal $soal)
    {
        $soal->delete();

        return redirect()->route('soal.index');
    }

    public function getScore($id)
    {

    }
}
