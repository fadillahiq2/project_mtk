<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $fillable = [
        'pertanyaan', 'a', 'b', 'c', 'd', 'e', 'jawaban_benar'
    ];


    public function materi()
    {
        return $this->belongsTo(Materi::class, 'materi_id', 'id');
    }

    public function soal_images()
    {
        return $this->hasMany(SoalImages::class, 'materi_id');
    }

}
