<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMateriIdToSubMateris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_materis', function (Blueprint $table) {
            $table->unsignedBigInteger('materi_id');

            $table->foreign('materi_id')->references('id')->on('materis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_materis', function (Blueprint $table) {
            $table->dropForeign(['materi_id']);
            $table->dropColumn(['materi_id']);
        });
    }
}
