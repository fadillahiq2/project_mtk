<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MateriController@user')->name('materi.user');

Auth::routes();

Route::get('/profile', 'UserController@profile')->middleware('auth')->name('profile');
Route::post('/profile', 'UserController@update_photo')->middleware('auth')->name('update_photo');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('materis', 'MaterisController')->middleware('auth');
Route::resource('sub-materi', 'SubMateriController')->middleware('auth');
Route::resource('soal', 'SoalController')->middleware('auth');
Route::resource('materi-images', 'MateriImagesController')->middleware('auth');
Route::resource('sub-materi-images', 'SubMateriImagesController')->middleware('auth');
Route::resource('soal-images', 'SoalImagesController')->middleware('auth');
Route::resource('icons', 'IconController')->middleware('auth');

Route::get('/materi/{id}', 'MateriController@index')->name('materi');
Route::post('/materi/latihan/', 'MateriController@nilai')->name('materi.score');
Route::get('/materi/latihan/{id}', 'MateriController@soal')->name('materi.latihan');


Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

