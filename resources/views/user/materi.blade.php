<!DOCTYPE html>
<html lang="en">

@include('user.partials.head')

<body style="background:rgb(0, 132, 255)">
    @include('user.partials.header')
    <div class="container" style="margin-top: 50px; margin-bottom:50px;">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">



                        <h2 style="color: cornflowerblue;margin-bottom:15px;text-align:center;">{{ $materi->title }}</h2>

                        @foreach ($materi_images as $materi_image)
                            <p style="text-align:justify;"><img style="display: block; margin-left: auto; margin-right: auto;" src="{{ Storage::url($materi_image->image) }}"
                                    class="img-fluid" alt="" style="width: 500px;height:325px;float:inherit;">
                        @endforeach
                        @foreach ($sub_materi as $submateri)
                                {!!$submateri->description!!}
                        @endforeach

                                                <br>
                                                <a href="{{ url('/') }}" class="btn btn-danger" role="button"
                                                    aria-pressed="true">Kembali</a>
                                                <a href="{{ url('/materi/latihan/'.$materi->id) }}" class="btn btn-primary" role="button"
                                                    aria-pressed="true">Latihan</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright -->
    @include('user.partials.footer')
    <!-- General JS Scripts -->
    @include('user.partials.scripts')
    <!-- Page Specific JS File -->
</body>

</html>
