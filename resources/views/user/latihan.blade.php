<!DOCTYPE html>
<html lang="en">
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('user.partials.head')
<style>
    .pt-3 p{
        display: inline;
    }

    .form-check .form-check-label p{
        display: inline;
    }


</style>
<body style="background:rgb(0, 132, 255)">

    @include('user.partials.header')
    <div class="container" style="margin-top: 50px; margin-bottom:50px;">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h2 style="color: cornflowerblue;margin-bottom:15px;text-align:center;">Latihan Soal</h2>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form action="{{ route('materi.score') }}" method="POST">
                        @csrf
                        @forelse($soal as $s)
                        <h5 class="pt-3">{{ $loop->iteration }}. {!!$s->pertanyaan !!}</h5>
                        <div class="form-check">
                            <label class="form-check-label" for="exampleRadios1">
                            A. {!! $s->a !!}
                            </label>
                        </div>
                        <div class="form-check">

                            <label class="form-check-label" for="exampleRadios2">
                            B. {!! $s->b !!}
                            </label>
                        </div>
                        <div class="form-check">

                            <label class="form-check-label" for="exampleRadios3">
                            C. {!! $s->c !!}
                            </label>
                        </div>
                        <div class="form-check">

                            <label class="form-check-label" for="exampleRadios4">
                            D. {!! $s->d !!}
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label" for="exampleRadios5">
                            E. {!! $s->e !!}
                            </label>
                        </div>
                        <input type="hidden" name="url" value="{{ $s-> materi_id }}">
                        <input type="hidden" name="jawaban{{ $loop->iteration }}" value="{!! $s-> jawaban_benar !!}">
                        <select name="pilihan{{ $loop->iteration }}" class="form-control" >
                            <option value="" disabled selected>Pilih Jawaban</option>
                            <option value="a">A</option>
                            <option value="b">B</option>
                            <option value="c">C</option>
                            <option value="d">D</option>
                            <option value="e">E</option>
                        </select>
                        @empty
                            <tr>
                                <td colspan="6" style="color: red;" align="center">Data Empty!</td>
                            </tr>
                        @endforelse
                        <br>
                        <button type="submit" class="btn btn-danger float-right mb-1">Selesai</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(session()->has('success'))

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Horeee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--FORM TAMBAH BARANG-->
                    <form action="" method=" ">
                        <div class="form-group">

                            <h6 for="">Kamu telah mengerjakan soal dengan nilai :</h6>
                            <h3 style="color:rgb(48, 189, 255)">{{session('success')}}</h3>

                        </div>
                        <a href="{{ url('/materis/latihan/'.$materi->id) }}" class="btn btn-danger" role="button" aria-pressed="true">Latihan Lagi</a>
                        <a href="{{ url('/') }}" class="btn btn-primary" role="button" aria-pressed="true">Selesai</a>
                    </form>
                    <!--END FORM TAMBAH BARANG-->
                </div>
            </div>

        </div>
     </div>
     @endif

    <!-- Copyright -->
    @include('user.partials.footer')
    <!-- General JS Scripts -->
    @include('user.partials.scripts')

    <!-- Page Specific JS File -->
</body>

</html>
{{-- <script type="text/javascript">


var hasilCekJawaban = {
    benar : 0,
    salah : 0,
}
    $(document).ready(function() {
        $("#submit").click(function(e){
            e.preventDefault();
                var datastring = $("#err").serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/materis/latihan",
                type:'POST',
                data: datastring,
                dataType: "json",
                success: function(result){
                    if(result.benar) {
                    hasilCekJawaban.benar + 1
                    console.log(hasilCekJawaban.benar)
                    }else{
                    hasilCekJawaban.salah + 1
                    }
                }
            });


        });

        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });
</script> --}}


