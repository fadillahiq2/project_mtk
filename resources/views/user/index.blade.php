<!DOCTYPE html>
<html lang="en">

    @include('user.partials.head')

<body style="background:rgb(0, 132, 255)">

    @include('user.partials.header')
    <div class="container" style="margin-top: 50px; margin-bottom:50px;">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <p style="text-align:justify;"><img src="{{ asset('assets_user/img/animasi_matematika.jpg') }}"
                                class="img-fluid" alt="" style="width: 500px;height:325px;float:left;">
                            <h2 style="color: cornflowerblue;margin-bottom:15px">Apa itu Matematika ?</h2>
                            <h6 style="line-height: 1.5">Secara etimologi, matematika berasal dari bahasa Yunani Kuno
                                (Ancient Greek) yaitu máthēma yang berarti pengetahuan, ilmu pengetahuan, dan belajar.
                                Matematika adalah ilmu pengetahuan tentang kuantitas, struktur, ruang, dan perubahan.
                            </h6>
                            <h6 style="line-height: 1.5">Disini kita bisa belajar 15 Materi Matematika yang tertera
                                dibawah. Silahkan melakukan pembelajaran ataupun mengerjakan soal, dengan mengklik materi
                                yang telah tersedia dibawah ini...</h6>
                        </p>
                        </h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="margin-top: 50px;">
            <section class="section">
                <div class="section-body">

                    <div class="row">
                        @foreach ($materi as $materi)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">

                            <div class="card card-statistic-1">
                                {!! $materi->icon !!}
                                <div class="card-wrap mx-auto">
                                    <div class="card-header">
                                        <a href="{{ route('materi',$materi->id) }}">
                                            <h4>{{ $materi->title }}</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @endforeach
                    </div>

                </div>
            </section>
        </div>
    </div>
    <!-- Copyright -->
    @include('user.partials.footer')
    <!-- General JS Scripts -->
    @include('user.partials.scripts')

    <!-- Page Specific JS File -->
</body>

</html>
