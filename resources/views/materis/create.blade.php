@extends('admin.template.default')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-custom">
                        <div class="card-header">
                        <h3 class="card-title">
                        Materi Inputs
                        </h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ route('materis.store') }}" method="POST">
                            @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Title</label>
                                <div class="col-10">
                                    <input class="form-control" name="title" type="text" placeholder="Title" id="example-text-input"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Icon</label>
                                <div class="col-1">
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-info">
                                        <i class="fa fa-superscript text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/bilanganpangkat.png') }}" alt="bilangan pangkat">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-danger">
                                        <i class="far fa-chart-bar text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/persamaanlinier.png') }}" alt="persamaan linear">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-warning">
                                        <i class="fas fa-square-root-alt text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/matriks.png') }}" alt="matriks">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-success">
                                        <i class="fas fa-sort-numeric-up" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/fungsi.png') }}" alt="fungsi">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-danger">
                                        <i class="fas fa-arrows-alt-h text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/barisderet.png') }}" alt="barisan deret">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-warning">
                                        <i class="fas fa-chart-line text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/program linear.png') }}" alt="program linear">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-success">
                                        <i class="fas fa-ruler-combined text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/trigonometri.png') }}" alt="trigonometri">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-info">
                                        <i class="fas fa-shapes text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/geometri.png') }}" alt="geometri">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-warning">
                                        <i class="fab fa-accusoft text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/transformasi.png') }}" alt="transformasi">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-success">
                                        <i class="fas fa-adjust text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/lingkaran.png') }}" alt="lingkaran">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-info">
                                        <i class="fas fa-dice-five text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/peluang.png') }}" alt="peluang">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-danger">
                                        <i class="fas fa-chart-pie text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/statistika.png') }}" alt="statistika">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-success">
                                        <i class="fas fa-infinity text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/limit.png') }}" alt="limit">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-info">
                                        <i class="fas fa-calculator text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/turunan.png') }}" alt="turunan">
                                    <br><br>
                                    <input class="form-check-input" type="radio" name="icon" value='
                                    <div class="card-icon bg-danger">
                                        <i class="fab fa-figma text-white" style="font-size: 2rem;"></i>
                                    </div>
                                    '>
                                    <img src="{{ asset('assets_user/icon/integral.png') }}" alt="integral">
                                </div>
                            </div>

                        <div class="card-footer">
                        <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{ route('materis.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
