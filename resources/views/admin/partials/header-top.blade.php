<div class="header-top">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Left-->
        <div class="d-none d-lg-flex align-items-center mr-3">
            <!--begin::Logo-->
            <a href="{{ route('home') }}" class="mr-20">
                <img alt="Logo" src="{{ asset('assets/theme/html/demo7/dist/assets/media/logos/logo-letter-9.png') }}" class="max-h-35px" />
            </a>
            <!--end::Logo-->
            <!--begin::Tab Navs(for desktop mode)-->
            <ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
                <!--begin::Item-->
                <li class="nav-item">
                    <a href="#" class="nav-link py-4 px-6 active" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">Home</a>
                </li>
                <!--end::Item-->
                <!--begin::Item-->

                <!--end::Item-->
                <!--begin::Item-->

                <!--end::Item-->
                <!--begin::Item-->

                <!--end::Item-->
            </ul>
            <!--begin::Tab Navs-->
        </div>
        <!--end::Left-->
        <!--begin::Topbar-->
        <div class="topbar bg-primary">
            <!--begin::Search-->

            <!--end::Search-->
            <!--begin::Notifications-->

            <!--end::Notifications-->
            <!--begin::Quick Actions-->
            <!--end::Quick Actions-->
            <!--begin::My Cart-->

            <!--end::My Cart-->
            <!--begin::Quick panel-->

            <!--end::Quick panel-->
            <!--begin::Chat-->

            <!--end::Chat-->
            <!--begin::User-->
            <div class="topbar-item">
                <div class="btn btn-icon btn-hover-transparent-white w-sm-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    <div class="d-flex flex-column text-right pr-sm-3">
                        <span class="text-white opacity-50 font-weight-bold font-size-sm d-none d-sm-inline">{{ Auth::user()->name }}</span>
                        <span class="text-white font-weight-bolder font-size-sm d-none d-sm-inline">Admin</span>
                    </div>
                    <span class="symbol symbol-35">
                        <span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-30">M</span>
                    </span>
                </div>
            </div>
            <!--end::User-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>
