@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <img src="uploads/photo/{{ $user->photo }}" style="width: 150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px;" alt="">
            <h2>{{ $user->name }} Profile</h2>
            <label for="email">Email : </label>
            <p id="email" style="display: inline">{{ $user->email }}</p>
            <br>
            <label for="gender">Gender : </label>
            <p id="gender" style="display: inline;">{{ $user->gender }}</p>
            <form enctype="multipart/form-data" action="{{ route('update_photo') }}" method="post">
                @csrf
                <label>Update Profile Image</label>
                <br>
                <input type="file" name="photo">
                <button type="submit" class="pull-right btn btn-sm btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
