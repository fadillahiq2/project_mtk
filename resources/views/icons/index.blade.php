{{-- @extends('admin.template.default')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Local Data</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Crud</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Icons</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">List Icons Images</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <a href="{{ route('icons.create') }}" class="btn btn-light-primary font-weight-bolder btn-sm">Add New</a>
                <!--end::Actions-->
                <!--begin::Dropdown-->
                <!--end::Dropdown-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">List Icons
                        <span class="text-muted pt-2 font-size-sm d-block">Icons yang telah dibuat</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->

                        <!--end::Dropdown-->
                        <!--begin::Button-->

                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Search Form-->
                    <!--begin::Search Form-->
                    <div class="mb-7">
                        <div class="row align-items-center">
                            <div class="col-lg-9 col-xl-8">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Search Form-->
                    <!--end: Search Form-->
                    <!--begin: Datatable-->
                    <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded"
                    id="kt_datatable" style="position: static; zoom: 1;">
                    <table class="datatable-table" style="display: block;">
                        <thead class="datatable-head">
                            <tr class="datatable-row" style="left: 0px;">
                                <th data-field="OrderID" class="datatable-cell"><span
                                        style="width: 112px;">Icons ID</span></th>
                                <th data-field="OrderID" class="datatable-cell"><span
                                        style="width: 112px;">Class</span></th>
                                <th data-field="Type" class="datatable-cell">
                                    <span style="width: 112px;">Image</span>
                                </th>
                                <th data-field="Actions" class="datatable-cell">
                                    <span style="width: 125px;">Actions</span></th>
                            </tr>
                        </thead>
                        <tbody class="datatable-body" style="">
                            @foreach ($icons as $icon)
                            <tr data-row="0" class="datatable-row" style="left: 0px;">
                                <td class="datatable-cell"><span style="width: 112px;">#{{ $icon->id }}</span></td>
                                <td class="datatable-cell"><span style="width: 112px;">{{ $icon->class }}</span></td>
                                <td class="datatable-cell"><img src="{{ Storage::url($icon->image) }}" alt="" width="112px" /></td>
                                <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">

                                    <span style="overflow: visible; position: relative; width: 125px;">
                                        <form action="{{ route('icons.destroy',$icon->id) }}" method="POST">
                                        <a href="{{ route('icons.edit',$icon->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                            <span class="svg-icon svg-icon-md"> <svg
                                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path
                                                            d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z"
                                                            fill="#000000" fill-rule="nonzero"
                                                            transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) ">
                                                        </path>
                                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2"
                                                            rx="1"></rect>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                            <span class="svg-icon svg-icon-md"> <svg
                                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path
                                                            d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                            fill="#000000" fill-rule="nonzero"></path>
                                                        <path
                                                            d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                            fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </button>
                                    </form>
                                    </span>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="datatable-pager datatable-paging-loaded">
                        <ul class="datatable-pager-nav my-2 mb-sm-0">
                            <li><a title="First"
                                    class="datatable-pager-link datatable-pager-link-first datatable-pager-link-disabled"
                                    data-page="1" disabled="disabled"><i class="flaticon2-fast-back"></i></a></li>
                            <li><a title="Previous"
                                    class="datatable-pager-link datatable-pager-link-prev datatable-pager-link-disabled"
                                    data-page="1" disabled="disabled"><i class="flaticon2-back"></i></a></li>
                            <li style="display: none;"><input type="text" class="datatable-pager-input form-control"
                                    title="Page number"></li>
                            <li><a class="datatable-pager-link datatable-pager-link-number datatable-pager-link-active"
                                    data-page="1" title="1">1</a></li>
                            <li><a class="datatable-pager-link datatable-pager-link-number" data-page="2" title="2">2</a></li>
                            <li><a class="datatable-pager-link datatable-pager-link-number" data-page="3" title="3">3</a></li>
                            <li><a class="datatable-pager-link datatable-pager-link-number" data-page="4" title="4">4</a></li>
                            <li><a class="datatable-pager-link datatable-pager-link-number" data-page="5" title="5">5</a></li>
                            <li><a title="Next" class="datatable-pager-link datatable-pager-link-next" data-page="2"><i
                                        class="flaticon2-next"></i></a></li>
                            <li><a title="Last" class="datatable-pager-link datatable-pager-link-last" data-page="20"><i
                                        class="flaticon2-fast-next"></i></a></li>
                        </ul>
                    </div>
                </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection



 --}}
